import 'package:flutter/material.dart';

import 'package:easy_video_ad/easy_video_ad.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  EasyVideoAd.putInMain();

  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String adEvent;
  bool immerSive;

  @override
  initState() {
    super.initState();
    adEvent = "nothing";
    immerSive = false;

    EasyVideoAd.onAdEventTriggered = (String event) {
      setState(() {
        adEvent = event;
      });
    };

    EasyVideoAd.onAdRewarded =
        (int rewardedAmount, String rewardedType, String rewardedItem) {
      print(
          "\n----------------------------onAdRewarded----------------------------------\n");
      print(
          "rewardedAmount: $rewardedAmount\rewardedType: $rewardedType\rewardedItem: $rewardedItem\n");
    };
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: SafeArea(
          child: Container(
            width: double.infinity,
            child: Column(
              children: <Widget>[
                Text(adEvent),
                RaisedButton(
                  onPressed: EasyVideoAd.loadTestAd,
                  child: Text("LoadTestAd"),
                ),
                RaisedButton(
                  onPressed: EasyVideoAd.forceLoadTestAd,
                  child: Text("forceLoadTestAd"),
                ),
                RaisedButton(
                  onPressed: EasyVideoAd.showAdIfLoaded,
                  child: Text("showAdIfLoaded"),
                ),
                RaisedButton(
                  onPressed: EasyVideoAd.showAd,
                  child: Text("showAd"),
                ),
                RaisedButton(
                  onPressed: () {
                    immerSive = !immerSive;
                    EasyVideoAd.setImmersiveMode(immerSive);
                  },
                  child: Text("showAd"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
