# easy_video_ad

This plugin gives you a simple way to use the RewardedVideoAd.
Google's 'com.google.firebase:firebase-ads:19.1.0' library is used for the RewardedVideoAd.

## Getting Started

You have to modifiy the AndroidManifest.xml in the following way for this to work:

### 1)
    Add the following lines in the <manifest/> tag:

    <uses-permission android:name="android.permission.INTERNET" />
    <uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />

### 2)
    Add the following lines in the <application/> tag, infront of the <activity/> tag:

    <!-- Sample AdMob App ID: ca-app-pub-3940256099942544~3347511713 -->
    <meta-data
        android:name="com.google.android.gms.ads.APPLICATION_ID"
        android:value="ca-app-pub-3940256099942544~3347511713" />
    
