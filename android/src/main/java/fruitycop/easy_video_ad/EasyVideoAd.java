package fruitycop.easy_video_ad;
//VERSION 2.1
//stable


import android.content.Context;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardedVideoAd;

import java.util.concurrent.atomic.AtomicBoolean;

import io.flutter.Log;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

class EasyVideoAd {
    private RewardedVideoAd rewardedVideoAd;
    private final AdRequest adBuilder;
    private String videoID;
    private final Context mainContext;
    private final BinaryMessenger messenger;
    private final String LOG_TAG = EasyVideoAd.class.getSimpleName();
    private final MethodChannel channel;
    private AtomicBoolean isInitialized = new AtomicBoolean(false);

    EasyVideoAd(Context mainContext, BinaryMessenger messenger, MethodChannel channel) {
        this.mainContext = mainContext;
        this.messenger = messenger;
        this.channel = channel;
        this.adBuilder = new AdRequest.Builder().build();
        this.videoID = Constants.SAMPLE_rewardedVideoAdID;
        this.isInitialized.set(false);

        initMobileAd();
    }

    private void initMobileAd() {
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(mainContext);
        Log.i(LOG_TAG, "Set RewardedVideoAdListener");
        rewardedVideoAd.setRewardedVideoAdListener(
                new EasyVideoAdListener(messenger)
        );
        isInitialized.set(true);

        Log.i(LOG_TAG, "RewardedVideoAd initialized, now invoking 'onInitializationComplete'");
        channel.invokeMethod("onInitializationComplete", null);

    }

    /*******************************************************************************
     * TITLE: lifecycle methods
     * AUTHOR: ObeE
     * DESCRIPTION:
     ********************************************************************************/
    private void pause() {
        Log.i(LOG_TAG, "Pause");
        rewardedVideoAd.pause(mainContext);
    }

    private void resume() {
        Log.i(LOG_TAG, "Resume");
        rewardedVideoAd.resume(mainContext);
    }

    private void destroy() {
        Log.i(LOG_TAG, "Destroy");
        rewardedVideoAd.destroy(mainContext);
    }

    /*******************************************************************************
     * TITLE: loadAd in various ways
     * AUTHOR: ObeE
     * DESCRIPTION:
     ********************************************************************************/
    //loadAd with this.videoID
    //this.videoID is by default Constants.SAMPLE_rewardedVideoAdID
    private void loadAd(MethodChannel.Result result) {
        if (!rewardedVideoAd.isLoaded()) {
            rewardedVideoAd.loadAd(
                    this.videoID,
                    adBuilder
            );
        } else {
            handleResultError(result, EasyVideoAdError.ALREADY_LOADED, null);
        }
    }

    private void loadAdWithVideoID(MethodChannel.Result result, String videoID) {
        if (!rewardedVideoAd.isLoaded()) {
            rewardedVideoAd.loadAd(
                    videoID,
                    adBuilder
            );
        } else {
            handleResultError(result, EasyVideoAdError.ALREADY_LOADED, null);
        }
    }

    //do not check if an ad is loaded, just execute rewardedVideoAd.loadAd()
    private void forceLoadAd(String videoID) {
        rewardedVideoAd.loadAd(
                videoID,
                adBuilder
        );

    }

    private void loadTestAd(MethodChannel.Result result) {
        if (!rewardedVideoAd.isLoaded()) {
            rewardedVideoAd.loadAd(
                    Constants.SAMPLE_rewardedVideoAdID,
                    adBuilder
            );
        } else {
            handleResultError(result, EasyVideoAdError.ALREADY_LOADED, null);
        }
    }

    /*******************************************************************************
     * TITLE: showAd in various ways
     * AUTHOR: ObeE
     * DESCRIPTION:
     ********************************************************************************/
    private void showAd() {
        rewardedVideoAd.show();
    }

    private void showAdIfLoaded(MethodChannel.Result result) {
        if (rewardedVideoAd.isLoaded()) {
            rewardedVideoAd.show();
        } else {
            handleResultError(result, EasyVideoAdError.NOT_LOADED, null);
        }
    }

    /*******************************************************************************
     * TITLE: get & set some values
     * AUTHOR: ObeE
     * DESCRIPTION:
     ********************************************************************************/
    //result.success(bool)
    private void isLoaded(MethodChannel.Result result) {
        result.success(rewardedVideoAd.isLoaded());
    }

    //result.success(String)
    private void getMediationAdapterClassName(MethodChannel.Result result) {
        try {
            rewardedVideoAd.getResponseInfo().getMediationAdapterClassName();
        } catch (Exception e) {
            result.error(
                    "NullPointerException",
                    " 'rewardedVideoAd.getResponseInfo().getMediationAdapterClassName(); ' produced a NullPointerException: " + e.getMessage(),
                    e);
        }
    }

    //result.success(String)
    private void getResponseId(MethodChannel.Result result) {
        try {
            rewardedVideoAd.getResponseInfo().getResponseId();
        } catch (Exception e) {
            Log.e(LOG_TAG, "getResponseId caused an Exception: " + e.getMessage());
            result.error(
                    "NullPointerException",
                    " 'rewardedVideoAd.getResponseInfo().getResponseId(); ' produced a NullPointerException: " + e.getMessage(),
                    e);
        }
    }

    //result.success(String)
    private void getCustomData(MethodChannel.Result result) {
        result.success(rewardedVideoAd.getCustomData());
    }

    private void setCustomData(String customData) {
        rewardedVideoAd.setCustomData(customData);
    }

    //result.success(String)
    private void getUserId(MethodChannel.Result result) {
        result.success(rewardedVideoAd.getUserId());
    }

    private void setUserId(String userId) {
        rewardedVideoAd.setUserId(userId);
    }

    private void setImmersiveMode(boolean immersiveMode) {
        rewardedVideoAd.setImmersiveMode(immersiveMode);
    }

    private void setRewardedVideoAd() {
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(mainContext);
    }

    private void setVideoId(String videoID) {
        this.videoID = videoID;
    }

    /*******************************************************************************
     * TITLE: Methodcall stuff
     * AUTHOR: ObeE
     * DESCRIPTION:
     ********************************************************************************/
    void onMethodCall(MethodCall call, MethodChannel.Result result) {
        if (isInitialized.get()) {
            switch (call.method) {
                case "pause":
                    pause();
                    break;
                case "resume":
                    resume();
                    break;
                case "destroy":
                    destroy();
                    break;
                case "loadAd":
                    loadAd(result);
                    break;
                case "loadAdWithVideoID":
                    final String loadAdWithVideoID_arg_videoID = call.argument("loadAdWithVideoID_arg_videoID");
                    loadAdWithVideoID(result, loadAdWithVideoID_arg_videoID);
                    break;
                case "forceLoadAd":
                    final String forceLoadAd_arg_videoID = call.argument("forceLoadAd_arg_videoID");
                    forceLoadAd(forceLoadAd_arg_videoID);
                    break;
                case "loadTestAd":
                    loadTestAd(result);
                    break;
                case "showAd":
                    showAd();
                    break;
                case "showAdIfLoaded":
                    showAdIfLoaded(result);
                    break;
                case "isLoaded":
                    isLoaded(result);
                    break;
                case "getMediationAdapterClassName":
                    getMediationAdapterClassName(result);
                    break;
                case "getResponseId":
                    getResponseId(result);
                    break;
                case "getCustomData":
                    getCustomData(result);
                    break;
                case "setCustomData":
                    final String setCustomData_arg_customData = call.argument("setCustomData_arg_customData");
                    setCustomData(setCustomData_arg_customData);
                    break;
                case "getUserId":
                    getUserId(result);
                    break;
                case "setUserId":
                    final String setUserId_arg_userId = call.argument("setUserId_arg_userId");
                    setUserId(setUserId_arg_userId);
                    break;
                case "setImmersiveMode":
                    final boolean setImmersiveMode_arg_immersiveMode = call.argument("setImmersiveMode_arg_immersiveMode");
                    setImmersiveMode(setImmersiveMode_arg_immersiveMode);
                    break;
                case "setRewardedVideoAd":
                    setRewardedVideoAd();
                    break;
                case "setVideoId":
                    final String setVideoId_arg_videoID = call.argument("setVideoId_arg_videoID");
                    setVideoId(setVideoId_arg_videoID);
                    break;
                default:
                    result.notImplemented();
            }
        } else {
            handleResultError(result, EasyVideoAdError.NOT_INITIALIZED, rewardedVideoAd);
        }
    }

    /*******************************************************************************
     * TITLE: EasyVideoAdError stuff
     * AUTHOR: ObeE
     * DESCRIPTION:
     ********************************************************************************/
    public enum EasyVideoAdError {
        ALREADY_LOADED("1", "The RewardedVideoAd has already been loaded."),
        NOT_LOADED("2", "The RewardedVideoAd has to be loaded first"),
        NOT_INITIALIZED("3", "The rewardedVideoAd has not been initialized yet");

        private final String errorCode;
        private final String errorMessage;

        EasyVideoAdError(String errorCode, String errorMessage) {
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
        }
    }

    private void handleResultError(MethodChannel.Result result, EasyVideoAdError error, Object errorDetails) {
        Log.e(LOG_TAG, "ErrorCode: " + error.errorCode + "\n" + "ErrorMessage: " + error.errorMessage);
        result.error(error.errorCode, error.errorMessage, errorDetails);
    }

    /*******************************************************************************
     * TITLE: Status
     * AUTHOR: ObeE
     * DESCRIPTION:
     ********************************************************************************/

    public enum Status {

    }
}

//public class EasyVideoAd {
//    private static final String EasyRewardedVideoAd_CHANNEL =
//            "easy_ads/EasyRewardedVideoAd_CHANNEL";
//    private static final String EasyRewardedVideoAdListener_CHANNEL =
//            "easy_ads/EasyRewardedVideoAdListener_CHANNEL";
//
//    private static String class_TAG = EasyVideoAd.class.getSimpleName();
//    private static Handler handler = new Handler();
//    private static RewardedVideoAd rewardedVideoAd = null;
//    private static EasyVideoAdListener listener = new EasyVideoAdListener();
//    private static AdRequest adBuilder;
//    private static String TestVideoID = "ca-app-pub-3940256099942544/5224354917";
//    private static String adVideoId;
//    static final String Test_AdMobID = "ca-app-pub-3940256099942544~3347511713";
//    private static boolean isInitialized = false;
//    private static Activity mainActivity;
//
//    public static void executeOnceInOnCreate(Context mainContext, FlutterEngine flutterEngine, String adMobId, String adVideoId) {
//        InitMobileAds initMobileAds = new InitMobileAds();
//        initMobileAds.execute(mainContext, adMobId);
//        EasyVideoAd.adVideoId = adVideoId;
//
//        initMethodChannel(call, result, flutterEngine, mainContext);
//        initEventChannel(flutterEngine);
//    }
//
//    public static void executeOnceInOnCreate(MethodCall call, MethodChannel.Result result,Context mainContext, FlutterEngine flutterEngine) {
//        InitMobileAds initMobileAds = new InitMobileAds();
//        initMobileAds.execute(mainContext, Test_AdMobID);
//        adVideoId = TestVideoID;
//
//        initMethodChannel(call, result, flutterEngine, mainContext);
//        initEventChannel(flutterEngine);
//    }
//
//    private static boolean isInitialized() {
//        return isInitialized;
//    }
//
//    private static void initialize(RewardedVideoAd rewardedVideoAdInstance, MethodChannel.Result result) {
//        if (!isInitialized) {
//            rewardedVideoAd = rewardedVideoAdInstance;
//            setListener(listener);
//            adBuilder = new AdRequest.Builder().build();
//            isInitialized = true;
//        } else {
//            Log.i(mainActivity.getClass().getSimpleName(), "EasyRewardedVideoAd is already initialized");
//            result.error(
//                    "already_initialized",
//                    "EasyRewardedVideoAd is already initialized",
//                    null
//            );
//        }
//    }
//
//    protected static void initialize(RewardedVideoAd rewardedVideoAdInstance) {
//        if (!isInitialized) {
//            rewardedVideoAd = rewardedVideoAdInstance;
//            setListener(listener);
//            adBuilder = new AdRequest.Builder().build();
//            isInitialized = true;
//        } else {
//            Log.i(mainActivity.getClass().getSimpleName(), "EasyRewardedVideoAd is already initialized");
//        }
//    }
//
//    private static void changeMobileAds(Context context, String adMobId) {
//        rewardedVideoAd.destroy(context);
//
//        InitMobileAds initMobileAds = new InitMobileAds();
//        initMobileAds.execute(context, adMobId);
//    }
//
//    private static void setListener(final RewardedVideoAdListener myListener) {
//        new Thread(
//                new Runnable() {
//                    @Override
//                    public void run() {
//                        for (int i = 0; i < 3; i++) {
//                            handler.post(new Runnable() {
//                                @Override
//                                public void run() {
//                                    try {
//                                        rewardedVideoAd.setRewardedVideoAdListener(myListener);
//                                        Log.i(class_TAG, "setRewardedVideoAdListener: success");
//                                    } catch (Exception e) {
//                                        Log.e(class_TAG, "setRewardedVideoAdListener: " + e.getMessage());
//                                        e.printStackTrace();
//                                    }
//                                }
//                            });
//
//
//                            try {
//                                Thread.sleep(50);
//                            } catch (Exception e) {
//                                Log.e(class_TAG, "sleep: " + e.getMessage());
//                                e.printStackTrace();
//                            }
//                        }
//                    }
//                }
//
//
//        ).start();
//
//
//    }
//private static void initEventChannel(FlutterEngine flutterEngine) {
//    new EventChannel(
//            flutterEngine.getDartExecutor().getBinaryMessenger(),
//            EasyRewardedVideoAdListener_CHANNEL
//    ).setStreamHandler(
//            new EventChannel.StreamHandler() {
//                @Override
//                public void onListen(Object args, final EventChannel.EventSink events) {
//                    listener.setEvents(events);
//                }
//
//                @Override
//                public void onCancel(Object args) {
//                }
//            }
//    );
//}
//}