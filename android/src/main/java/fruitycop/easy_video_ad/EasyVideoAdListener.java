package fruitycop.easy_video_ad;

import android.util.Log;

import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;

import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.EventChannel;

class EasyVideoAdListener implements RewardedVideoAdListener {
    private String LOG_TAG = EasyVideoAdListener.class.getSimpleName();
    private EventChannel.EventSink easyVideoAdEvents;
    private final BinaryMessenger messenger;
    private final String EasyVideoAdListener_EventChannel = "easy_video_ad_listener";

    EasyVideoAdListener(BinaryMessenger messenger) {
        this.messenger = messenger;

        initEventChannel();
    }

    private void initEventChannel() {
        new EventChannel(
                messenger,
                EasyVideoAdListener_EventChannel
        ).setStreamHandler(
                new EventChannel.StreamHandler() {
                    @Override
                    public void onListen(Object args, final EventChannel.EventSink events) {
                        easyVideoAdEvents = events;
                    }

                    @Override
                    public void onCancel(Object args) {
                    }
                }
        );
    }

    /*******************************************************************************
     * TITLE: onEvent methods
     * AUTHOR: ObeE
     * DESCRIPTION:
     ********************************************************************************/

    @Override
    public void onRewardedVideoAdLoaded() {
        Log.i(LOG_TAG, "onRewardedVideoAdLoaded");
        easyVideoAdEvents.success("onRewardedVideoAdLoaded");
    }

    @Override
    public void onRewardedVideoAdOpened() {
        Log.i(LOG_TAG, "onRewardedVideoAdOpened");
        easyVideoAdEvents.success("onRewardedVideoAdOpened");
    }

    @Override
    public void onRewardedVideoStarted() {
        Log.i(LOG_TAG, "onRewardedVideoStarted");
        easyVideoAdEvents.success("onRewardedVideoStarted");
    }

    @Override
    public void onRewardedVideoAdClosed() {
        Log.i(LOG_TAG, "onRewardedVideoAdClosed");
        easyVideoAdEvents.success("onRewardedVideoAdClosed");
    }

    @Override
    public void onRewarded(RewardItem rewardItem) {
        Log.i(LOG_TAG, "onRewarded\nrewardedItem: " + rewardItem.toString() + "\nrewardedAmount: " + rewardItem.getAmount() + "\n" + "rewardedType: " + rewardItem.getType());
        easyVideoAdEvents.success("onRewarded;" + rewardItem.toString() + ";" + rewardItem.getAmount() + ";" + rewardItem.getType());
    }

    @Override
    public void onRewardedVideoAdLeftApplication() {
        Log.i(LOG_TAG, "onRewardedVideoAdLeftApplication");
        easyVideoAdEvents.success("onRewardedVideoAdLeftApplication");
    }

    @Override
    public void onRewardedVideoAdFailedToLoad(int i) {
        Log.i(LOG_TAG, "onRewardedVideoAdFailedToLoad\nerrorCode: " + i);
        easyVideoAdEvents.success("onRewardedVideoAdFailedToLoad;" + i);
    }

    @Override
    public void onRewardedVideoCompleted() {
        Log.i(LOG_TAG, "onRewardedVideoCompleted");
        easyVideoAdEvents.success("onRewardedVideoCompleted");
    }


}
