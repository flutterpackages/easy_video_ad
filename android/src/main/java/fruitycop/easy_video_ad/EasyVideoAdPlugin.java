package fruitycop.easy_video_ad;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.embedding.engine.plugins.activity.ActivityAware;
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * EasyVideoAdPlugin
 */
public class EasyVideoAdPlugin implements FlutterPlugin, MethodCallHandler {
    private static final String LOG_TAG = EasyVideoAdPlugin.class.getSimpleName();

    private static EasyVideoAd easyVideoAdInstance;

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {
        final MethodChannel channel = new MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "easy_video_ad");
        channel.setMethodCallHandler(new EasyVideoAdPlugin());

        easyVideoAdInstance = new EasyVideoAd(
                flutterPluginBinding.getApplicationContext(),
                flutterPluginBinding.getBinaryMessenger(),
                channel
        );
    }

    // This static function is optional and equivalent to onAttachedToEngine. It supports the old
    // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
    // plugin registration via this function while apps migrate to use the new Android APIs
    // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
    //
    // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
    // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
    // depending on the user's project. onAttachedToEngine or registerWith must both be defined
    // in the same class.
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "easy_video_ad");
        channel.setMethodCallHandler(new EasyVideoAdPlugin());

        easyVideoAdInstance = new EasyVideoAd(
                registrar.context(),
                registrar.messenger(),
                channel
        );
    }

    @Override
    public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {
        Log.i(LOG_TAG, "onMethodCall: " + call.method);
        if (easyVideoAdInstance != null) {
            easyVideoAdInstance.onMethodCall(call, result);
        } else {
            Log.e(LOG_TAG, "EasyVideoAd has not been constructed yet");
        }
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    }


}
