#import "EasyVideoAdPlugin.h"
#if __has_include(<easy_video_ad/easy_video_ad-Swift.h>)
#import <easy_video_ad/easy_video_ad-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "easy_video_ad-Swift.h"
#endif

@implementation EasyVideoAdPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftEasyVideoAdPlugin registerWithRegistrar:registrar];
}
@end
